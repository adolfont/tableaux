# DSL

## Requisitos "Lógicos"

- Cada fórmula deve ter sua origem
  - {premissa, número-de-ordem} ou
  - {regra, ramo-origem, fórmula-origem-no-ramo}

## Requisitos de uma representação linear de uma árvore

- Parte 1: nós
- Parte 2: ligações entre os nós

## Decisões

- Cada nós pode ter uma ou mais fórmulas


## Exemplos

Sequente: A|B, A->C, B->D |- C|D

# Aqui ficam os ramos
:r1
1. T A|B   {premissa, 1}
2. T A->C  {premissa, 2}
3. T B->D  {premissa, 3}
4. F C|D   {premissa, 4}

:r2
1. T A  {T|, left, {:r1, 1}}

:r3
1. T B  {T|, right, {}:r1, 1}}

# Colocar sempre na ordem de criação

:r4
1. F A {T->, left, {:r1, 2}}
2. x   {closing, {:r2, 1}, {:r4, 1}}

:r5
1. T C {T->, right, {:r1, 2}}

:r6
1. F B

:r7

# Ligações entre os ramos
:r1 -> :r2
:r1 -> :r3
:r2 -> :r4
:r2 -> :r5
:r5 -> :r6
:r5 -> :r7
