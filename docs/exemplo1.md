# Exemplo 1

Prove
A, A->B |- B

1. T A      [premissa]
2. T A->B   [premissa]
3. F B      [conclusão]
  4. F A      [T->, 2]
  6. x        [1,4]

  5. T B      [T->, 2]
  6. x        [3,5]
Resultado: sequente provado

Checador verifica e diz que as linhas acima são sim uma prova do sequente A, A->B |- B

# Exemplo 2

Prove
A|B, A->C, B->D |- C|D

1. T A|B   [premissa]
2. T A->C  [premissa]
3. T B->D  [premissa]
4. F C|D   [premissa]
5. F C     [F|, 4]
6. F D     [F|, 4]
  7. T A   [T|, 1]
    9. F A [T->, 2]
    11. x [7,9]
    10. T C [T->, 2]
    12. x [5,10]
  8. T B   [T|, 1]
    13. F B [T->, 3]
    15. x [8,13]
    14. T D [T->, 3]
    16. x [6,14]

# Exemplo 2 usando a ideia de recursividade

Prove
A|B, A->C, B->D |- C|D

1. T A|B   [premissa]
2. T A->C  [premissa]
3. T B->D  [premissa]
4. F C|D   [premissa]
5. F C     [F|, 4]
6. F D     [F|, 4]
---> bifurca aqui
7. T A  [T|, 1]
---> bifurca aqui
8. F A  [T->, 2]
9. x  [7,8]

Outra estrutura
Pilha
8. T C [T->, 2]
7. T B [T|, 1]

# Mais uma ideia

Separar atômicas, lineares e bifurcantes

A|B, A->C, B->D |- C|D


# Observações

Que tal usar a ideia de recursividade??? Complicado...

Ou de expansão...

Não fica claro quando fechou o tablô, como fica na versão árvore.c

Parece ser complicado bifurcar e depois colocar fórmulas no meio.
E a questão da numeração também.
E devo colocar espaço?

Como verificar?

Verificar se as premissas são tradução correta do sequente.

Verificar se cada linha veio corretamente do sequente mais aplicações de regras mais fórmulas obtidas.

Verificar se o fechamento está correto.

Verificar se os ramos fechados foram fechados.

E as valorações contra-exemplo?

Verificar indentação?

Verificar se não tem repetição, reaplicação de regras?
