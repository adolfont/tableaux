I teach Logic for Computer Science to undergraduate students.

My students are first-year undegraduate students in Computer Engineering or Information Systems.

This semester I did a little bit of an experiment of introducing them to Elixir.

My goal was not to teach them Elixir, but mainly to enable them to use Elixir to learn logic.

So, first I had to decide which data structure to use to represent logical formulas.

Using Elixir's basic data structures, I had two choices: lists and tuples.

I chose tuples, because formulas do not increase in size.

So, the representation would be like this:

https://github.com/adolfont/logica_classica_proposicional
