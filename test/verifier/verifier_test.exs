defmodule VerifierTest do
  use ExUnit.Case

  setup do
    {:ok,
     [
       first_sequent: {nil, ["T !!A", "F A", "F !A", "T A", "x"], nil},
       first_result: "[[\"T !!A\", \"F A\", \"F !A\", \"T A\", \"x\"]]",
       simple_test_1: {{nil, [4], nil}, [1, 2, 3], {nil, [5], nil}},
       simple_test_1_result: "[[1, 2, 3, 4], [1, 2, 3, 5]]",
       simple_test_2: {{nil, [4], nil}, [1, 2, 3], nil},
       simple_test_2_result: "[[1, 2, 3, 4]]",
       simple_test_3: {nil, [1, 2, 3], {nil, [4], nil}},
       simple_test_3_result: "[[1, 2, 3, 4]]",
       height_3_test_1:
         {{{nil, [6], nil}, [4], {nil, [7], nil}}, [1, 2, 3],
          {{nil, [8], nil}, [5], {nil, [9], nil}}},
       height_3_test_1_result:
         "[[1, 2, 3, 4, 6], [1, 2, 3, 4, 7], [1, 2, 3, 5, 8], [1, 2, 3, 5, 9]]",
       large_test:
         {{{nil, [6], nil}, [4], {nil, [7], nil}}, [1, 2, 3],
          {{nil, [8], nil}, [5], {nil, [9], nil}}},
       large_test_result:
         "[[1, 2, 3, 4, 6], [1, 2, 3, 4, 7], [1, 2, 3, 5, 8], [1, 2, 3, 5, 9]]"
     ]}
  end

  test "Showing the tableaux part of the representation for a proof of !!A|-A",
       context do
    assert inspect(Verifier.showAllBranches(context[:first_sequent])) ==
             context[:first_result]
  end

  test "Showing all branches of simple test #1", context do
    assert inspect(Verifier.showAllBranches(context[:simple_test_1])) ==
             context[:simple_test_1_result]
  end

  test "Showing all branches of simple test #2", context do
    assert inspect(Verifier.showAllBranches(context[:simple_test_2])) ==
             context[:simple_test_2_result]
  end

  test "Showing all branches of simple test #3", context do
    assert inspect(Verifier.showAllBranches(context[:simple_test_3])) ==
             context[:simple_test_3_result]
  end

  test "Showing all branches of height_3_test_1", context do
    assert inspect(Verifier.showAllBranches(context[:height_3_test_1])) ==
             context[:height_3_test_1_result]
  end

  test "Large test", context do
    assert inspect(Verifier.showAllBranches(context[:large_test])) ==
             context[:large_test_result]
  end
end
