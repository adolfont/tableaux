defmodule TableauxTest do
  use ExUnit.Case

  ### Find Rule

  test "The rule for T !A is T!" do
    assert Tableaux.findRule({:t, {:not, :a}}) == {:t, :not}
  end

  test "The rule for F !A is F!" do
    assert Tableaux.findRule({:f, {:not, :a}}) == {:f, :not}
  end

  test "The rule for T (A&B) is T&" do
    assert Tableaux.findRule({:t, {:a, :and, :a}}) == {:t, :and}
  end

  test "The rule for T (A|B) is T|" do
    assert Tableaux.findRule({:t, {:a, :or, :a}}) == {:t, :or}
  end

  test "There is no rule for T A" do
    assert Tableaux.findRule({:t, :a}) == nil
  end

  ### Expand

  test "The expansion of T A gives  []" do
    assert Tableaux.expandLinear({:t, :a}) == []
  end

  test "The expansion of T !A gives  F A" do
    assert Tableaux.expandLinear({:t, {:not, :a}}) == [{:f, :a}]
  end

  test "The expansion of F !A gives  T A" do
    assert Tableaux.expandLinear({:f, {:not, :a}}) == [{:t, :a}]
  end

  test "The expansion of T (A&B) gives  T A, T B" do
    assert Tableaux.expandLinear({:t, {:a, :and, :b}}) == [{:t, :a}, {:t, :b}]
  end

  test "The expansion of F (A|B) gives  F A, F B" do
    assert Tableaux.expandLinear({:f, {:a, :or, :b}}) == [{:f, :a}, {:f, :b}]
  end

  test "The expansion of F (A->B) gives  T A, F B" do
    assert Tableaux.expandLinear({:f, {:a, :implies, :b}}) == [
             {:t, :a},
             {:f, :b}
           ]
  end

  test "The expansion with branching of T A gives  []" do
    assert Tableaux.expandBranching({:t, :a}) == []
  end

  test "The expansion of F (A&B) gives  F A /\ F B" do
    assert Tableaux.expandBranching({:f, {:a, :and, :b}}) == [
             {:f, :a},
             {:f, :b}
           ]
  end

  test "The expansion of T (A|B) gives  T A /\ T B" do
    assert Tableaux.expandBranching({:t, {:a, :or, :b}}) == [{:t, :a}, {:t, :b}]
  end

  test "The expansion of T (A->B) gives  F A /\ T B" do
    assert Tableaux.expandBranching({:t, {:a, :implies, :b}}) == [
             {:f, :a},
             {:t, :b}
           ]
  end

  ### Select first linear formula
  test "The first linear formula in T A, T C&B, F !D is T C&B" do
    assert Tableaux.firstLinearFormula([
             {:t, :a},
             {:t, {:c, :and, :b}},
             {:f, {:not, :d}}
           ]) == {:t, {:c, :and, :b}}
  end

  test "There is no linear formula in T A, F C&B" do
    assert Tableaux.firstLinearFormula([
             {:t, :a},
             {:f, {:c, :and, :b}}
           ]) == nil
  end

  ### Select first branching formula

  test "The first branching formula in T A, F C&B, T C|D is F C&B" do
    assert Tableaux.firstBranchingFormula([
             {:t, :a},
             {:f, {:c, :and, :b}},
             {:t, {:c, :or, :d}}
           ]) == {:f, {:c, :and, :b}}
  end

  test "There is no branching formula in T A, F !(C&B)" do
    assert Tableaux.firstBranchingFormula([
             {:t, :a},
             {:f, {:not, {:c, :and, :b}}}
           ]) == nil
  end

  ### Closing

  test "The [T A, F A] branch is closed" do
    assert Tableaux.closed?([{:t, :a}, {:f, :a}]) == true
  end

  test "The [T A, F B] branch is not closed (=open)" do
    assert Tableaux.closed?([{:t, :a}, {:f, :b}]) == false
  end

  test "The [... T C->D ... F C->D] branch is closed" do
    assert Tableaux.closed?([
             {:t, :a},
             {:f, :b},
             {:t, {:c, :implies, :d}},
             {:f, :x},
             {:f, {:c, :implies, :d}}
           ]) == true
  end

  test "Select all linear formulas in [T A, T A->B, F !!A]" do
    assert Tableaux.selectAllLinear([
             {:t, :a},
             {:f, {:a, :implies, :b}},
             {:f, {:not, {:not, :a}}}
           ]) ==
             [
               {:f, {:a, :implies, :b}},
               {:f, {:not, {:not, :a}}}
             ]
  end

  test "Expand F !!A recursively" do
    assert Tableaux.expandLinearRecursively([
             {:f, {:not, {:not, :a}}}
           ]) == [{:t, {:not, :a}}, {:f, :a}]
  end

  test "Expand T (!A&B) recursively" do
    assert Tableaux.expandLinearRecursively([
             {:t, {{:not, :a}, :and, :b}}
           ]) == [{:t, {:not, :a}}, {:t, :b}, {:f, :a}]
  end

  test "Expand T (!A&!!B) recursively" do
    assert Tableaux.expandLinearRecursively([
             {:t, {{:not, :a}, :and, {:not, {:not, :b}}}}
           ]) == [
             {:t, {:not, :a}},
             {:t, {:not, {:not, :b}}},
             {:f, :a},
             {:f, {:not, :b}},
             {:t, :b}
           ]
  end

  ### Expand Branch Linear
  test "The [T A, F !!A] branch is expanded with linear rules to [T A, F !!A, T !A, F A]" do
    assert Tableaux.expandBranchLinear([{:t, :a}, {:f, {:not, {:not, :a}}}]) ==
             [{:t, :a}, {:f, {:not, {:not, :a}}}, {:t, {:not, :a}}, {:f, :a}]
  end

  test "The [T A, T A->B, F !!A] branch is expanded with linear rules to ..." do
    assert Tableaux.expandBranchLinear([
             {:t, :a},
             {:t, {:a, :implies, :b}},
             {:f, {:not, {:not, :a}}}
           ]) ==
             [
               {:t, :a},
               {:t, {:a, :implies, :b}},
               {:f, {:not, {:not, :a}}},
               {:t, {:not, :a}},
               {:f, :a}
             ]
  end

  test "The [T A&B, F !!B] branch is expanded with linear rules to ..." do
    assert Tableaux.expandBranchLinear([
             {:t, {:a, :and, :b}},
             {:f, {:not, {:not, :c}}}
           ]) ==
             [
               {:t, {:a, :and, :b}},
               {:f, {:not, {:not, :c}}},
               {:t, :a},
               {:t, :b},
               {:t, {:not, :c}},
               {:f, :c}
             ]
  end

  ### Select Branching Formulas

  test "The [T A, T A->B] branch contains branching rules ..." do
    assert Tableaux.selectBranching([{:t, :a}, {:t, {:a, :implies, :b}}]) ==
             [{:t, {:a, :implies, :b}}]
  end

  test "The [T A, T A->B, T C|D] branch contains branching rules ..." do
    assert Tableaux.selectBranching([
             {:t, :a},
             {:t, {:a, :implies, :b}},
             {:t, {:c, :or, :d}}
           ]) == [{:t, {:a, :implies, :b}}, {:t, {:c, :or, :d}}]
  end

  ### Create tableau from sequent
  test "The sequent A, A->B |- B is converted into [T A, T A->B, F B] ..." do
    assert Tableaux.convertToSignedFormulas(["A", "A->B", "B"]) ==
             "T A, T A->B, F B"
  end

  ### Expand Branch Branching

  # TODO
  # Applies only one rule, the first branching rule.
  # How to register(record) that the formula was used???
  #  Remove the formula that was used?
  # Work with only one branch at a time???
  #  How to record the saved branch??llll
  # T A->B --> F A, T B
end
