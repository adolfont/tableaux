Definitions.

L     = [A-Za-z]
NU     = [0-9_]
WHITESPACE = [\s\t\n\r]

Rules.

{L}+{NU}*     : {token, {atom, TokenLine, to_atom(TokenChars)}}.
\(            : {token, {'(',  TokenLine}}.
\)            : {token, {')',  TokenLine}}.
!             : {token, {'not',  TokenLine, 'not'}}.
\|            : {token, {'or',  TokenLine, 'or'}}.
&             : {token, {'and',  TokenLine, 'and'}}.
->            : {token, {'implies',  TokenLine, 'implies'}}.
{WHITESPACE}+ : skip_token.

Erlang code.

to_atom(Chars) ->
    list_to_atom(Chars).
