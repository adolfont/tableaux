# Tableaux

A software to help students learn how to write proofs using the signed [analytic tableaux method](https://en.wikipedia.org/wiki/Method_of_analytic_tableaux). See also: [Rules for Signed Analytic Tableaux for Classical Propositional Logic](http://www.dainf.ct.utfpr.edu.br/~adolfo/Disciplinas/LogicaParaComputacao_novo/MateriaisPorAssunto/4.SistemasDedutivos/4.1.TablosAnaliticos/SignedAnalyticTableaux.pdf)

## Installation

First you have to install [Elixir](https://elixir-lang.org/install.html). Then, using [git](https://git-scm.com/) on a terminal, do:

`git clone https://gitlab.com/adolfont/tableaux`

and

`mix start`

## Demo

Watch [tableaux's video demo](https://youtu.be/OZw9zf2137g) to see how it works.

## License


Tableaux is released under the [MIT License](https://opensource.org/licenses/MIT).
