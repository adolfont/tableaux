defmodule Verifier do
  defp showAllBranchesAux({nil, root, nil}, current) do
    [current ++ root]
  end

  defp showAllBranchesAux({left, root, nil}, current) do
    showAllBranchesAux(left, current ++ root)
  end

  defp showAllBranchesAux({nil, root, right}, current) do
    showAllBranchesAux(right, current ++ root)
  end

  defp showAllBranchesAux({left, root, right}, current) do
    x = showAllBranchesAux(left, current ++ root)
    y = showAllBranchesAux(right, current ++ root)
    x ++ y
  end

  def showAllBranches({left, root, right}) do
    showAllBranchesAux({left, root, right}, [])
  end
end
