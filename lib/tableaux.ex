defmodule Tableaux do
  def findRule({sign, {connective, _}}) do
    {sign, connective}
  end

  def findRule({sign, {_, connective, _}}) do
    {sign, connective}
  end

  def findRule(_) do
  end

  def opposite_sign(:t), do: :f
  def opposite_sign(:f), do: :t

  def expandLinear({sign, {:not, subformula}}) do
    [{opposite_sign(sign), subformula}]
  end

  def expandLinear({:t, {left_subf, :and, right_subf}}) do
    [{:t, left_subf}, {:t, right_subf}]
  end

  def expandLinear({:f, {left_subf, :or, right_subf}}) do
    [{:f, left_subf}, {:f, right_subf}]
  end

  def expandLinear({:f, {left_subf, :implies, right_subf}}) do
    [{:t, left_subf}, {:f, right_subf}]
  end

  def expandLinear(_), do: []

  def expandBranching({:f, {left_subf, :and, right_subf}}) do
    [{:f, left_subf}, {:f, right_subf}]
  end

  def expandBranching({:t, {left_subf, :or, right_subf}}) do
    [{:t, left_subf}, {:t, right_subf}]
  end

  def expandBranching({:t, {left_subf, :implies, right_subf}}) do
    [{:f, left_subf}, {:t, right_subf}]
  end

  def expandBranching(_), do: []

  def closed?([{sign, formula} | rest]) do
    Enum.member?(rest, {opposite_sign(sign), formula}) or closed?(rest)
  end

  def closed?(_) do
    false
  end

  def selectAllLinear(branch) do
    Enum.filter(branch, &isLinear/1)
  end

  def expandLinearRecursively([]), do: []

  def expandLinearRecursively([formula]) do
    case expandLinear(formula) do
      [] ->
        []

      [x] ->
        [x] ++ expandLinearRecursively([x])

      [x, y] ->
        [x, y] ++ expandLinearRecursively([x]) ++ expandLinearRecursively([y])
    end
  end

  defp expandEachFormula([head | tail]) do
    expandLinearRecursively([head]) ++ expandEachFormula(tail)
  end

  defp expandEachFormula([]), do: []

  def expandBranchLinear(branch) do
    branch ++ expandEachFormula(branch)
  end

  def selectBranching([formula = {:f, {_, :and, _}} | rest]) do
    [formula] ++ selectBranching(rest)
  end

  def selectBranching([formula = {:t, {_, :or, _}} | rest]) do
    [formula] ++ selectBranching(rest)
  end

  def selectBranching([formula = {:t, {_, :implies, _}} | rest]) do
    [formula] ++ selectBranching(rest)
  end

  def selectBranching([_formula | rest]) do
    selectBranching(rest)
  end

  def selectBranching([]) do
    []
  end

  defp isLinear({_, {:not, _}}), do: true
  defp isLinear({:t, {_, :and, _}}), do: true
  defp isLinear({:f, {_, :or, _}}), do: true
  defp isLinear({:f, {_, :implies, _}}), do: true
  defp isLinear(_), do: false

  def firstLinearFormula([head | tail]) do
    if isLinear(head) do
      head
    else
      firstLinearFormula(tail)
    end
  end

  def firstLinearFormula(_), do: nil

  defp isBranching({:f, {_, :and, _}}), do: true
  defp isBranching({:t, {_, :or, _}}), do: true
  defp isBranching({:t, {_, :implies, _}}), do: true
  defp isBranching(_), do: false

  def firstBranchingFormula([head | tail]) do
    if isBranching(head) do
      head
    else
      firstBranchingFormula(tail)
    end
  end

  def firstBranchingFormula(_), do: nil

  def convertToSignedFormulas([last]) do
    "F " <> last
  end

  def convertToSignedFormulas([head | tail]) do
    "T " <> head <> ", " <> convertToSignedFormulas(tail)
  end
end
