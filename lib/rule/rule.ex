defmodule Rule do
  defp pretty_printing_sign(:t), do: "T"
  defp pretty_printing_sign(:f), do: "F"

  defp pretty_printing_connective(:and), do: "&"
  defp pretty_printing_connective(:or), do: "|"
  defp pretty_printing_connective(:not), do: "!"
  defp pretty_printing_connective(:implies), do: "->"

  def pretty_printing_rule({sign, connective}) do
    pretty_printing_sign(sign) <> pretty_printing_connective(connective)
  end

  def pretty_printing_rule(nil), do: "none"
end
