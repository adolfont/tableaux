defmodule Formula do
  @pretty_printing_symbols %{
    :not => "!",
    :and => "&",
    :or => "|",
    :implies => "->"
  }

  def pretty_printing_formula(formula) when is_atom(formula) do
    Atom.to_string(formula)
  end

  def pretty_printing_formula({unary_connective, subformula}) do
    @pretty_printing_symbols[unary_connective] <>
      pretty_printing_formula(subformula)
  end

  def pretty_printing_formula({lsubformula, connective, rsubformula}) do
    "(" <>
      pretty_printing_formula(lsubformula) <>
      @pretty_printing_symbols[connective] <>
      pretty_printing_formula(rsubformula) <> ")"
  end

  def pretty_printing_signed_formula({:t, formula}) do
    "T " <> pretty_printing_formula(formula)
  end

  def pretty_printing_signed_formula({:f, formula}) do
    "F " <> pretty_printing_formula(formula)
  end
end
