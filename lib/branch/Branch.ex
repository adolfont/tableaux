defmodule Branch do
  def pretty_printing_branch(branch) do
    for x <- branch do
      IO.puts(Formula.pretty_printing_signed_formula(x))
    end
  end

  def generate_branch(a_branch) do
    for x <- a_branch do
      Parser.parse_signed_formula(x)
    end
  end

  def string_for_pretty_printing_branch(branch, separator \\ "\n") do
    formulas =
      for x <- branch do
        Formula.pretty_printing_signed_formula(x)
      end

    Enum.join(formulas, separator)
  end
end
