defmodule ConvertSequentToSignedFormulas do
  import Tableaux.Util
  @sequent_formulas [
    "A",
    "B",
    "(A&B)",
    "(B|A)",
    "(!A->C)",
    "!!C",
    "(C->(A&!B))"
  ]

  def sortFormulas() do
    shuffled_formulas = @sequent_formulas |> Enum.shuffle()
    size = length(shuffled_formulas)
    Enum.take(shuffled_formulas, 2 + Enum.random(0..(size - 2)))
  end

  def showAsSequent(list) do
    Enum.join(Enum.take(list, length(list) - 1), ", ") <>
      " ⊢ " <> (Enum.take(list, -1) |> Enum.at(0))
  end

  def get_answer() do
    # String.trim(IO.gets("Type your answer as signed formulas separated by commas:\n"))
    IO.puts(
      "Type your answer, one signed formula per line. To finish, type '0',:"
    )

    Enum.join(multilineread("0", ""), ", ")
  end

  def perform(state) do
    IO.puts(" ")
    question_head = "Convert the following sequent to signed formulas:"
    IO.puts(question_head)
    sequent = sortFormulas()
    question_content = showAsSequent(sequent)
    IO.puts(question_content)

    student_answer = get_answer()

    message =
      QuestionVerifier.check_answer(
        student_answer,
        Tableaux.convertToSignedFormulas(sequent)
      )

    question = question_head <> "\n" <> question_content <> "\n"

    QuestionVerifier.write_message_to_file(
      state,
      question,
      student_answer,
      message
    )

    IO.puts("Your answer is #{message}!")
    QuestionVerifier.change_state(message, state) |> Mix.Tasks.Start.menu()
  end
end
