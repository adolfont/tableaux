defmodule ExpandBranchLinearOption do
  import Tableaux.Util

  defp generate_question(a_branch) do
    question_head =
      "What formulas are generated from expanding the following branching using all linear formulas?"

    question_branch = Branch.string_for_pretty_printing_branch(a_branch)

    question_head <> "\n" <> question_branch
  end

  def get_answer() do
    IO.puts(
      "Type your answer, one signed formula per line. To finish, type '0',:"
    )

    Enum.join(multilineread("0", ""), ", ")
  end

  def performOption(state) do
    formulas_for_branch = [
      "T A",
      "T D",
      "T !!B",
      "F (A->C)",
      "T (B&C)",
      "F (A|C)",
      "T (A->B)",
      "F (B&D)",
      "T (D|!A)"
    ]

    size = length(formulas_for_branch)

    a_branch =
      Enum.take(
        Enum.shuffle(formulas_for_branch),
        1 + Enum.random(1..(size - 3))
      )

    a_branch = Branch.generate_branch(Enum.shuffle(a_branch))

    question = generate_question(a_branch)
    IO.puts(question)
    student_answer = get_answer()

    expanded_branch = Tableaux.expandBranchLinear(a_branch)

    answer =
      Branch.string_for_pretty_printing_branch(
        expanded_branch -- a_branch,
        ", "
      )

    message =
      QuestionVerifier.check_answer(
        student_answer,
        answer
      )

    QuestionVerifier.write_message_to_file(
      state,
      question,
      student_answer,
      message
    )

    IO.puts("Your answer is #{message}!")
    QuestionVerifier.change_state(message, state) |> Mix.Tasks.Start.menu()
  end
end
