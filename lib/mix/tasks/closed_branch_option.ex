defmodule ClosedBranchOption do
  import Tableaux.Util
  def performOption(state) do
    formulas_for_branch = [
      "T A",
      "T B",
      "F A",
      "T (A->B)",
      "F (A->C)",
      "T !(A&B)",
      "F !(B&A)",
      "T (A->C)"
    ]

    size = length(formulas_for_branch)

    a_branch =
      Enum.take(
        Enum.shuffle(formulas_for_branch),
        1 + Enum.random(1..(size - 3))
      )

    a_branch = Branch.generate_branch(Enum.shuffle(a_branch))

    question_head = "Is the following branch closed?"
    IO.puts(question_head)
    question_content = Branch.string_for_pretty_printing_branch(a_branch)
    IO.puts(question_content)
    student_answer = String.trim(IO.gets("Type your answer (Yes/No): "))

    message =
      QuestionVerifier.check_answer(
        student_answer,
        yes_to_true_false_to_no(Tableaux.closed?(a_branch))
      )

    question = question_head <> "\n" <> question_content
    IO.puts("Your answer is #{message}!")

    QuestionVerifier.write_message_to_file(
      state,
      question,
      student_answer,
      message
    )

    QuestionVerifier.change_state(message, state) |> Mix.Tasks.Start.menu()
  end
end
