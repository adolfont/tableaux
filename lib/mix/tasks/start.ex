defmodule Mix.Tasks.Start do
  use Mix.Task


  def main(_args) do
    run("Start")
  end

  def bye({right_answers, wrong_answers, file}) do
    IO.puts("#{right_answers} right answers.")
    IO.puts("#{wrong_answers} wrong answers.")
    IO.puts("Bye!")
    IO.binwrite(file, "#{right_answers} right answers.\n")
    IO.binwrite(file, "#{wrong_answers} wrong answers.\n")
    File.close(file)
  end

  def findRuleOptionResultsFunction(input) do
    Rule.pretty_printing_rule(Tableaux.findRule(input))
  end

  def findRuleOption(state) do
    tests = [
      "T A",
      "T B",
      "T !A",
      "F !A",
      "T (A&B)",
      "T (A|B)",
      "T (A->B)",
      "F (A&B)",
      "F (A|B)",
      "F (A->B)",
      "T !(A->B)",
      "T (!A->B)",
      "F !(A->B)",
      "F (!A&B)",
      "T (A->(B&C))",
      "F (!(A|B)&B)"
    ]

    answer_generator_function = &findRuleOptionResultsFunction/1

    question_message = "What is the rule for "

    configuration = {tests, answer_generator_function, question_message}

    QuestionVerifier.verify(state, configuration) |> menu
  end

  def expandLinearOptionResultsFunction(input) do
    results =
      for i <- Tableaux.expandLinear(input) do
        Formula.pretty_printing_signed_formula(i)
      end

    Enum.join(results, ", ")
  end

  def expandLinearOption(state) do
    tests = [
      "T !A",
      "F !A",
      "T (A&B)",
      "F (A|B)",
      "F (A->B)",
      "T !(A->B)",
      "F !(A->B)",
      "F (!(A|B)->B)"
    ]

    answer_generator_function = &expandLinearOptionResultsFunction/1

    question_message = "What is/are the conclusion(s) for "

    configuration = {tests, answer_generator_function, question_message}

    QuestionVerifier.verify(state, configuration) |> menu
  end

  def expandBranchingOptionResultsFunction(input) do
    results =
      for i <- Tableaux.expandBranching(input) do
        Formula.pretty_printing_signed_formula(i)
      end

    Enum.join(results, "; ")
  end

  def expandBranchingOption(state) do
    tests = [
      "F (A&B)",
      "T (A|B)",
      "T (A->B)",
      "T (!(A|B)->B)",
      "F (C&(!(A|B)->B))",
      "T (!(A|B)|B)"
    ]

    answer_generator_function = &expandBranchingOptionResultsFunction/1

    question_message = "What are the conclusion(s) for "

    configuration = {tests, answer_generator_function, question_message}

    QuestionVerifier.verify(state, configuration) |> menu
  end

  def execute_option(option, state) do
    case Integer.parse(option) do
      {0, _} -> bye(state)
      {1, _} -> ConvertSequentToSignedFormulas.perform(state)
      {2, _} -> findRuleOption(state)
      {3, _} -> expandLinearOption(state)
      {4, _} -> expandBranchingOption(state)
      {5, _} -> ClosedBranchOption.performOption(state)
      {6, _} -> ExpandBranchLinearOption.performOption(state)
      {_, _} -> menu(state)
      :error -> menu(state)
    end
  end

  def menu(state) do
    IO.puts("")
    IO.puts("0 - End application")
    IO.puts("1 - Convert sequent to signed formulas")
    IO.puts("2 - Find rule")
    IO.puts("3 - Expand linear formula")
    IO.puts("4 - Expand branching formula")
    IO.puts("5 - Is this branch closed?")
    IO.puts("6 - Expand branch using all linear formulas")
    option = IO.gets("Choose an option: ")
    execute_option(option, state)
  end

  def run(_) do
    IO.puts("Analytic Tableaux App - 2018 - by Adolfo Neto")

    IO.puts(
      "Testing knowledge of analytic tableaux for Classical Propositional Logic"
    )

    {:ok, file} = File.open("results.txt", [:append])
    IO.binwrite(file, DateTime.to_string(DateTime.utc_now()) <> "\n")

    menu({0, 0, file})
  end
end
