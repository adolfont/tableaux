defmodule QuestionVerifier do
  import Parser
  import Formula

  @right "right"
  @wrong "wrong"

  def check_answer(a, a), do: @right
  def check_answer(_, _), do: @wrong

  def change_state(@right, {right_answers, wrong_answers, file}) do
    {right_answers + 1, wrong_answers, file}
  end

  def change_state(_, {right_answers, wrong_answers, file}) do
    {right_answers, wrong_answers + 1, file}
  end

  defp change_tests(tests, @right, a_test_as_list) do
    tests -- a_test_as_list
  end

  defp change_tests(tests, _, _), do: tests

  def check_continue_option("Yes"), do: true
  def check_continue_option(_), do: false

  defp generate_expected_result(tests, function) do
    signed_formula = parse_signed_formula(Enum.random(tests))
    signed_formula_as_string = pretty_printing_signed_formula(signed_formula)

    {function.(signed_formula), signed_formula_as_string}
  end

  def continue_or_stop(false, state, _), do: state

  def continue_or_stop(true, state, configuration),
    do: verify(state, configuration)

  def write_message_to_file(state, question, answer, message) do
    {_, _, file} = state
    IO.binwrite(file, "#{question}\n")
    IO.binwrite(file, "Answer: #{answer}\n")
    IO.binwrite(file, "Result: #{message}\n")
  end

  def verify(state, {[], _, _}) do
    IO.puts("You have passed all tests!")
    state
  end

  def verify(state, {tests, function, question_message}) do
    {expected_result, signed_formula_as_string} =
      generate_expected_result(tests, function)

    question = question_message <> "'#{signed_formula_as_string}'?"
    IO.puts(question)
    student_answer = String.trim(IO.gets("Type your answer: "))
    message = check_answer(student_answer, expected_result)

    write_message_to_file(state, question, student_answer, message)

    new_state = change_state(message, state)
    changed_tests = change_tests(tests, message, [signed_formula_as_string])
    IO.puts("Your answer is #{message}!")
    continue_answer = String.trim(IO.gets("Do you wish to continue?(Yes/No): "))

    continue_or_stop(
      check_continue_option(continue_answer),
      new_state,
      {changed_tests, function, question_message}
    )
  end
end
