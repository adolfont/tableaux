defmodule Tableaux.Util do
  def yes_to_true_false_to_no(true), do: "Yes"
  def yes_to_true_false_to_no(false), do: "No"
  def yes_to_true_false_to_no(_), do: "Erro"

  defp next(end_expression, end_expression, _, result) do
    Enum.reverse(result)
  end

  defp next(new_value, end_expression, message, result) do
    multilineread_aux(end_expression, message, [new_value | result])
  end

  defp multilineread_aux(end_expression, message, result) do
    next(String.trim(IO.gets(message)), end_expression, message, result)
  end

  def multilineread(end_expression, message) do
    multilineread_aux(end_expression, message, [])
  end
end
