defmodule Parser do
  @spec parse(binary) :: tuple
  def parse(str) do
    {:ok, tokens, _} = str |> to_charlist() |> :formula_lexer.string()
    :formula_parser.parse(tokens)
  end

  def parse_sign("T "), do: :t
  def parse_sign("F "), do: :f

  def parse_signed_formula(str) do
    {:ok, formula} = parse(String.slice(str, 2..-1))
    {parse_sign(String.slice(str, 0..1)), formula}
  end
end
